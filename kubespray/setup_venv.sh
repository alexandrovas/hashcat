#!/bin/bash
REPO_ROOT="`dirname \"$0\"`"
rm -rf $REPO_ROOT/venv
python3 -m venv $REPO_ROOT/venv
. $REPO_ROOT/venv/bin/activate
pip3 install --upgrade pip
pip3 install -r $REPO_ROOT/requirements.txt --force

locals {
  google_project = "atlantean-study-313118"
  google_region  = "europe-west3"
  google_zone    = "europe-west3-a"
}

provider "google" {
  project = local.google_project
  region  = local.google_region
  zone    = local.google_zone
}

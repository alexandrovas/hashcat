resource "google_compute_instance_group" "kubmst" {
  name = "kubmst"
  zone = local.google_zone

  instances = google_compute_instance.kubmst[*].self_link

  lifecycle {
    create_before_destroy = true
  }

  named_port {
    name = "k8s-dashboard-port"
    port = local.k8s_dashboard_port
  }
}

resource "google_compute_instance" "kubmst" {
  count        = 2
  name         = "kubmst${format("%02d", count.index + 1)}"
  machine_type = "e2-medium"
  zone         = local.google_zone
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
      size  = 20
    }
  }

  metadata = {
    sshKeys = local.ssh_pub_keys
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

resource "tls_private_key" "k8s-dashboard" {
  algorithm   = "RSA"
}

resource "tls_self_signed_cert" "k8s-dashboard" {
  key_algorithm   = tls_private_key.k8s-dashboard.algorithm
  private_key_pem = tls_private_key.k8s-dashboard.private_key_pem

  subject {
    common_name  = "kubernetes.dashboard"
    organization = "ACME Examples, Inc"
  }

  validity_period_hours = 20000

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

module "lb-https-k8s-dashboard" {
  source         = "GoogleCloudPlatform/lb-http/google"
  version        = "~> 4.4"
  name           = "k8s-dashboard-https"
  create_address = true
  project        = local.google_project
  ssl = true
  private_key = tls_private_key.k8s-dashboard.private_key_pem
  certificate = tls_self_signed_cert.k8s-dashboard.cert_pem
  backends = {
    default = {
      protocol               = "HTTPS"
      description            = "K8s dashboard"
      port                   = local.k8s_dashboard_port
      port_name              = "k8s-dashboard-port"
      timeout_sec            = 5
      enable_cdn             = false
      custom_request_headers = null
      security_policy        = null

      connection_draining_timeout_sec = null
      session_affinity                = null
      affinity_cookie_ttl_sec         = null
      health_check = {
        request_path        = "/"
        port                = local.k8s_dashboard_port
        check_interval_sec  = null
        timeout_sec         = null
        healthy_threshold   = null
        unhealthy_threshold = null
        host                = null
        logging             = null
      }
      log_config = {
        enable      = true
        sample_rate = 1.0
      }
      groups = [
        {
          group                        = google_compute_instance_group.kubmst.id
          balancing_mode               = null
          capacity_scaler              = null
          description                  = null
          max_connections              = null
          max_connections_per_instance = null
          max_connections_per_endpoint = null
          max_rate                     = null
          max_rate_per_instance        = null
          max_rate_per_endpoint        = null
          max_utilization              = null
        }
      ]
      iap_config = {
        enable               = false
        oauth2_client_id     = null
        oauth2_client_secret = null
      }
    }
  }
}

#!/usr/bin/env python3

import pika
from datetime import datetime


credentials = pika.PlainCredentials('admin', 'admin')
connection = pika.BlockingConnection(pika.ConnectionParameters('10.233.45.152', 5672, '/', credentials))
channel = connection.channel()

channel.queue_declare(queue='response')

start_time = datetime.now()

def callback(ch, method, properties, body):
    if body:
        print(body)
        print('time: %s' % (datetime.now() - start_time))
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume('response', callback)


channel.start_consuming()

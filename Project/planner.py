#!/usr/bin/env python3

from numpy.random import poisson
from time import sleep
import sqlite3
import pika
import json
import sys
import os

HASHCAT_IMAGE = 'registry.gitlab.com/alexandrovas/hashcat/hashcat:0.0.8'


def set_task():
    conn = sqlite3.connect('./hashes.db')

    cursor = conn.cursor()

    cursor.execute('select hash, mask, type from hashes where checked=0')
    fetched_hash = cursor.fetchone()

    print(f"Debug fetched row: ", fetched_hash)

    if not fetched_hash:
        return 1

    cursor.execute("update hashes set checked=1 where hash='%s'" % fetched_hash[0])

    conn.commit()

    credentials = pika.PlainCredentials('usr', 'secret_pass')
    connection = pika.BlockingConnection(pika.ConnectionParameters('10.233.45.152', 5672, '/', credentials))
    channel = connection.channel()
    channel.queue_declare(queue='hashcat')

    keyspace = os.popen(f"docker run --rm {HASHCAT_IMAGE} hashcat --keyspace -a 3 {fetched_hash[1]}").read()
    print(f"Debug keyspace: ", keyspace)

    part = round(int(keyspace)/2)

    task1 = json.dumps([fetched_hash[2], fetched_hash[1], fetched_hash[0], "0", str(part)])
    task2 = json.dumps([fetched_hash[2], fetched_hash[1], fetched_hash[0], str(part), "0"])
    channel.basic_publish(exchange='', routing_key='hashcat', body=task1)
    channel.basic_publish(exchange='', routing_key='hashcat', body=task2)

    connection.close()
    return 0

if __name__ == '__main__':
    conn = sqlite3.connect('./hashes.db')
    cursor = conn.cursor()
    cursor.execute("update hashes set checked=0")
    conn.commit()
    conn.close()
    lamb = input('Input intense: ')

    while True:
        interval = poisson(int(lamb))
        print('Next iteration will be after %s seconds' % interval)
        ret = set_task()
        if ret == 1:
            break
        sleep(interval)

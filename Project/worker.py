import pika
import json
import os


rabbit_user = os.environ.get('RABBITMQ_USER', 'admin')
rabbit_pass = os.environ.get('RABBITMQ_PASS', 'admin')
rabbit_addr = os.environ.get('RABBITMQ_ADDR')
rabbit_port = int(os.environ.get('RABBITMQ_PORT', '5672'))
credentials = pika.PlainCredentials(rabbit_user, rabbit_pass)
connection = pika.BlockingConnection(pika.ConnectionParameters(rabbit_addr, rabbit_port, '/', credentials))

channel = connection.channel()

channel.queue_declare(queue='hashcat')
channel.queue_declare(queue='response')

def callback(ch, method, properties, body):
    parametersArr = json.loads(body)
    print(" [x] Received %r" % (body,))
    result = ''
    with open('hash', 'w') as f:
        f.write(parametersArr[2])
    if parametersArr[3] == "0":
        result = os.popen('hashcat -m ' + parametersArr[0] + ' -a 3 -l ' + parametersArr[4] + ' --force --potfile-disable --quiet ./hash ' + parametersArr[1]).read()
    if parametersArr[4] == "0":
        result = os.popen('hashcat -m ' + parametersArr[0] + ' -a 3 -s ' + parametersArr[3] + ' --force --potfile-disable --quiet ./hash ' + parametersArr[1]).read()
    print(result)
    channel.basic_publish(exchange='', routing_key='response', body=result)
    ch.basic_ack(delivery_tag = method.delivery_tag)
    
channel.basic_qos(prefetch_count=1)
channel.basic_consume('hashcat', callback)

channel.start_consuming()

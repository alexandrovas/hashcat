## Установка и настройка gcloud

   ```bash
   # установка клиента gloud (https://cloud.google.com/sdk/docs/install#linux)
   ...
   # авторизация в Google
   gcloud auth application-default login
   # установка проекта по умолчанию
   gcloud config set project atlantean-study-31128
   ```

## Применение конфигурации terraform

1. Установить [Terraform](https://www.terraform.io/downloads.html)

2. Заменить в файле `variables.tf` пути к файлам ssh-ключей:

    ```hcl
    gce_ssh_pub_key_files = [
      "~/.ssh/id_rsa.pub",
      "./pubkey.pub"
    ]
    ```

3. Применить конфигурацию terraform:

   ```bash
   # удалить все машины (не выполнять!!!!)
   terraform destroy
   # инициализация проекта (нужна один раз)
   terraform init
   # применением конфигурации (если изменений в коде нет, ничего не в  облаке не меняется)
   terraform apply
   ```

   В выводе будет адреса kubernetes-узлов, а также адрес Web-интерфеса  RabbitMQ. Выглядит примерно вот так:

   ```hcl
   k8s_nodes = {
     "kubmst01" = "ansible_host=34.107.104.79 ip=10.156.0.5"
     "kubmst02" = "ansible_host=34.107.77.62 ip=10.156.0.3"
     "kubwrk01" = "ansible_host=35.198.147.223 ip=10.156.0.2"
     "kubwrk02" = "ansible_host=35.246.213.1 ip=10.156.0.4"
   }
   rabbitmq_admin_url = "http://34.117.5.151:80"
   ```

## Деплой кластера k8s с помощью kubespray

1. Активация venv

    ```bash
    # переход в каталог kubespray
    cd kubespray
    # создание venv
    ./setup_venv.sh
    # активация venv
    . ./venv/bin/activate
    ```

2. Внесение изменений в инвентори:

   - `kubespray/inventory/hashcat-cluster/inventory.ini` - общий список хостов (заполняем на основе вывода terraform)
   - `kubespray/inventory/hashcat-cluster/group_vars/k8s_cluster` - основные переменные кластера

3. Деплой кластера с помощью ansible:

    ```bash
    ansible-playbook -i inventory/mycluster/inventory.ini -u root --become cluster.yml --private-key ~/.ssh/id_rsa
    ```

    Для добавления нод в кластер необходимо внести изменения в инвентори и заново применить плейбук.

4. Деплой приложения hashcat

    Выполняем на мастере следующие команды:

    ```bash
    cd /root
    git clone https://gitlab.com/alexandrovas/hashcat
    cd hashcat/Project
    kubectl apply -f .
    ```

## Внесение изменений в Dockerfile

1. Вносим изменения в файл `Project/Dockerfile`
 
2. Выполняем в консоли:
 
    ```bash
    # добавляем коммит с со своим сообщением
    git commit -m 'your message' -a
    # отправляем в удаленный репозиторий изменения
    git push
    # добавляем тег (версию увеличиваем каждый раз)
    git tag -a 0.0.7
    # отправляем в удаленный репозиторий новый тег
    git push --tags
    ```

3. Заходим через браузер в репозиторий на сайте Gitlab.org и выбираем слева CI/CD -> Pipelines. Видим свой пайплайн и ждем завершения.
 
4. После успешного завершения пайплайна переходим в Packages & Registries -> Container Registry. Видим свой docker-image и соответствующий тег.
 
5. Вносим изменения в файл `Project/hashcat_daemonset.yaml` (указываем соответствующий образ и тег):
 
    ```yaml
    containers:
    - name: hashcat
      image: 'registry.gitlab.com/alexandrovas/hashcat/hashcat:0.0.7'
    ```

6. Сохраняем изменения и пушим в удаленную репу:
 
    ```bash
    # добавляем коммит с со своим сообщением
    git commit -m 'your message' -a
    # отправляем в удаленный репозиторий изменения
    git push
    ```

7. Заходим по ssh на k8s-мастер и применяем новый конфиг:
 
    ```bash
    # переходим в каталог проекта
    cd /root/hashcat/Project
    # скачиваем изменения
    git pull
    # применяем конфиг
    kubectl apply -f .
    ```

## Включение дашборда

1. Заходим на мастер и выполняем команду:

    ```bash
    kubectl -n kubernetes-dashboard edit service kubernetes-dashboard
    ```

    Меняем `type: ClusterIP` на `type: NodePort` и сохраняем файл.

2. Смотрим какой порт назначен для сервиса:

    ```bash
    kubectl -n kubernetes-dashboard get svc kubernetes-dashboard
    ```

    Порт должен быть > 30000.

3. Идем в конфигурацию terraform и указываем этот порт в файле `terraform-google/variables.tf`:
 
    ```hcl
    locals {
      ...
      k8s_dashboard_port = 31314
      ...
    }
    ```

4. Применяем конфигурацию terraform:
 
    ```bash
    terraform apply
    ```

    В конце вывода в консоли забираем адрес дашборда:

    ```hcl
    k8s_dashboard_url = "https://34.120.21.224:443"
    ```

5. Заходим на мастер, на который клонировали репозиторий и выполняем:

    ```bash
    # переходим в каталог с конфигами
    cd /root/hashcat/k8s-dashboard
    # применяем всё содержимое каталога
    kubectl apply -f .
    # получаем токен
    kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
    ```

    В консоли должен появиться токен, который нужно использовать при входе в Kubernetes Dashboard.

6. Заходим по адресу из пункта 5.4, выбираем способ аутентификации Token и указываем полученный токен.
